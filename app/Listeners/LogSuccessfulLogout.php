<?php
namespace App\Listeners;

use DB;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Logout;


class LogSuccessfulLogout
{
    /**
        * Create the event listener.
        *
        * @return void
        */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
        * Handle the event.
        *
        * @param  Logout  $event
        * @return void
        */
    public function handle(Logout $event)
    {
        $user = $event->user;
        DB::statement('call Proc_LogOut(?)',array($user->id));
        info("User with ID " . $user->id . " has logged out.");
    }
}
