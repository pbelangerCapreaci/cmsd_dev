<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Storage;

class FileController extends Controller {

    public function get($directory, $slug)
    {
        if(Storage::exists($directory . "/" . $slug))
        {
            $storagePath = storage_path('app/' . $directory . "/" . $slug);
            return response()->file($storagePath);
        } else {
            return 'No Such File';
        }
    }
}
