<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use PhpOffice\PhpSpreadsheet\Writer;
use PhpOffice\PhpSpreadsheet\Reader;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use LynX39\LaraPdfMerger\Facades\PdfMerger;
use Symfony\Component\HttpFoundation\Request as HttpFoundationRequest;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $userId = Auth::user()->id;
        $layout = DB::select('call Proc_AffichageRecherche(?)',array($userId));
        foreach ($layout as $value) {
            if($value->Type == 'List') {
                $list = DB::select('call proc_Lists(?,?)',array($userId, $value->ChampNomGen));
                $value->list = $list;
            }
        }
        $layoutByCategory = array();
        foreach ($layout as $value) {
            $layoutByCategory[$value->Categorie][] = $value;
        }

        $researchTypeAvailable = DB::select('call Proc_ResearchTypeAvailable(?)',array($userId));
        $researchType = array();
        foreach ($researchTypeAvailable as $value) {
            $researchType[$value->RechercheNumber] = $value;
        }
        $credit = DB::select('call Proc_SoldeCredit(?)', array($userId))[0];
        $limit = DB::select('call Proc_UserLimitDisplay(?)', array($userId))[0];
        return view('home',[
            'layoutByCategory' => $layoutByCategory,
            'researchType' => $researchType,
            'credit' => $credit,
            'limit' => $limit
        ]);
    }

    public function getResultCount(Request $request)
    {
        $params = $request->all();
        $userId = Auth::user()->id;
        $data = $params['filterData'];
        DB::select('call proc_Input(?,?,?,?)',array($userId, $data['ChampNomGen'], $data['min'], $data['max']));
        $count = DB::select('call Proc_CountResults(?)',array($userId));
        return $count[0]->count;
    }

    public function getListValues(Request $request)
    {
        $listId = $request->input('listId');
        $userId = Auth::user()->id;
        $list = DB::select('call proc_Lists(?,?)',array($userId, $listId));
        return $list;
    }

    public function displayResult(Request $request)
    {
        $userId = Auth::user()->id;
        $researchType = $request->input('ResearchType');
        $resultTableName = "ResultContent_" . $userId;
        $resultFields = DB::select('CALL Proc_ResultDisContent(?,?)', array($userId, $researchType));
        usort($resultFields, function($a, $b) {
            return $a->oOrder <=> $b->oOrder;
        });
        $results = DB::table($resultTableName)->get();
        foreach ($results as $result) {
            foreach ($resultFields as $resultField) {
                if($resultField->Type == 'IMG' || $resultField->Type == 'PDF')
                {
                    if(!Storage::exists('temp/' . $result->{$resultField->Champ}))
                    {
                        $FileName = 'Recherche/Files/' . $result->{$resultField->Champ};
                        if(Storage::disk('s3')->exists($FileName)) {
                            $FileGet = Storage::disk('s3')->get($FileName);
                            Storage::put('temp/' . $result->{$resultField->Champ}, $FileGet);
                        } else {
                            if($resultField->Type == 'IMG') {
                                $result->{$resultField->Champ} = 'Dummy.png';
                            } else {
                                $result->{$resultField->Champ} = 'Dummy.pdf';
                            }
                        }
                    }
                }
            }
        }
        $returnHTML = view('result')->with(compact('resultFields', 'results'))->render();
        $reportTypes = DB::select('CALL Proc_ReportTypeAvailable(?)', array($userId));
        $modelHTML = view('model')->with(compact('reportTypes'))->render();
        return response()->json(array('success' => true, 'resultHtml'=>$returnHTML, 'modelHtml' => $modelHTML));
    }

    public function selectResult(Request $request)
    {
        $userId = Auth::user()->id;
        $id = $request->input('id');
        DB::select('CALL Proc_select(?,?)', array($userId, $id));
        return response()->json(array('success' => true));
    }

    public function exportPage(Request $request)
    {
        $userId = Auth::user()->id;
        $resultTableName = "ResultContent_" . $userId;
        $selectedResult = DB::table($resultTableName)->where('selected', 1)->get();
        $selectedTypes = json_decode($request->input('exportType'));
        $credit = DB::select('call Proc_SoldeCredit(?)', array($userId))[0];
        $listActe = DB::select('call Proc_ListeActe(?)', array($userId));
        $actePDF = [];
        foreach ($listActe as $Acte) {
            $FileName = 'Recherche/Files/' . $Acte->FichierActe;
            if($Acte->FichierActe == null || !Storage::disk('s3')->exists($FileName))
            {
                $actePDF[$Acte->ObjID]['status'] = "disabled";
            } else {
                $actePDF[$Acte->ObjID]['status'] = "";
            }
            $actePDF[$Acte->ObjID]['file'] = $Acte->FichierActe;
        }

        $exportSelected = [];

        foreach ($selectedTypes as $selectedType) {
            if($selectedType->Type == 'Individuel')
            {
                foreach ($selectedResult as $Selected) {
                    $ObjID = $Selected->ObjID;
                    $Export = DB::select('CALL Proc_BuildReport(?,?,?)', array($userId, $ObjID, $selectedType->id));
                    $NomFichier = storage_path('app/Output/FicheBrute_' . $userId . '_' . $selectedType->id . '_' . $ObjID . '.xlsx');
                    $NomFichierPDF = storage_path('app/Output/FicheBrute_' . $userId . '_' . $selectedType->id . '_' . $ObjID . '.pdf');
                    $exportSelected []= ['reportType' => $selectedType->id, 'ObjID' => $ObjID];
                    $Rapport = array_column($Export, 'Rapport_XLSX');
                    $NomModele = $Rapport[1];
                    $AExporterModel = array_column($Export, 'AExporterModel');
                    $AExporterModelName = explode(',', $AExporterModel[0]);
                    //Set the model with layout and with Values
                    $ModelNameLayout = storage_path('Models/' . $NomModele . '_Layout.xlsx');
                    $ModelNameValue = storage_path('Models/' . $NomModele . '_Value.xlsx');
                    //Extract information from the BuildResult_$id
                    $Clefs = array_keys($Export);
                    $Clefs2 = array_keys($Export);
                    $FeuilleExport = array_column($Export, 'FeuilleAExporter');
                    $Cellule = array_column($Export, 'CelluleAExporter');
                    $HeightTarget = array_column($Export, 'Height');
                    $WidthTarget = array_column($Export, 'Width');
                    $Type = array_column($Export, 'TYPE');
                    $Valeur = array_column($Export, 'Valeur');
                    //Open the Model
                    $reader = new reader\Xlsx();
                    $reader2 = new reader\Xlsx();
                    $spreadsheet = $reader->load($ModelNameValue);
                    $spreadsheet2 = $reader->load($ModelNameLayout);
                    //Push each text field in the model with formula
                    foreach($Clefs as $Clef){
                        $TypeName = $Type[$Clef];
                        if ($TypeName == 'Text'){
                            $ValeurName = $Valeur[$Clef];
                            if (is_null($ValeurName) == False) {
                                $SheetName = $FeuilleExport[$Clef];
                                $CellName = $Cellule[$Clef];
                                $sheet = $spreadsheet->setActiveSheetIndexByName($SheetName);
                                $sheet->setCellValue($CellName, $ValeurName);
                            }
                        }
                    }
                    //CopyPasteValue in the layout
                    foreach($AExporterModelName as $AExporter){
                        $Sheet = $spreadsheet->setActiveSheetIndexByName($AExporter);
                        $Data = $spreadsheet->getActiveSheet()->ToArray(null, TRUE, FALSE, FALSE);
                        $Sheet2 = $spreadsheet2->setActiveSheetIndexByName($AExporter);
                        $spreadsheet2->getActiveSheet()->fromArray($Data);
                    }
                    foreach($Clefs2 as $Clef2){
                        $TypeName = $Type[$Clef2];
                        if ($TypeName == 'IMG'){
                            $ValeurName = $Valeur[$Clef2];
                            if (is_null($ValeurName) == False){
                                $SheetName = $FeuilleExport[$Clef2];
                                $CellName = $Cellule[$Clef2];
                                $Image = new Drawing();
                                $FileName = 'Recherche/Files/' . $ValeurName;
                                $FileNameLocal = 'temp/' . $ValeurName;
                                if(!Storage::exists($FileNameLocal))
                                {
                                    if(Storage::disk('s3')->exists($FileName)) {
                                        $FileGet = Storage::disk('s3')->get($FileName);
                                        Storage::put($FileNameLocal, $FileGet);
                                    } else {
                                        $FileNameLocal = 'Models/Dummy.png';
                                    }
                                }
                                $Image->setPath(storage_path("app/" . $FileNameLocal));
                                $Data = getimagesize(storage_path("app/" . $FileNameLocal));
                                //Set the height or width to fit to a maximum the size required in the model while keeping proportions of the original image
                                $HeightTargetName = $HeightTarget[$Clef2];
                                $WidthTargetName = $WidthTarget[$Clef2];
                                $RatioTarget = $HeightTargetName / $WidthTargetName;
                                $Width = $Data[0];
                                $Height = $Data[1];
                                $Ratio = $Height / $Width;
                                if($Ratio > $RatioTarget){
                                    //If the ratio height to width from the image is higher than the model desire we fit the height
                                    $Image->setHeight($HeightTargetName);
                                }
                                else{
                                    //If the ratio height to width from the image is lower thant the model desire we fit the width.
                                    //Doing so insure that the image will fit the space in the model and will be as big as possible.
                                    $Image->setWidth($WidthTargetName);
                                }
                                $Image->setCoordinates($CellName);
                                $spreadsheet2-> setActiveSheetIndexByName($SheetName);
                                $Image->setWorksheet($spreadsheet2->getActiveSheet());
                            }
                        }
                    }

                    //Save printing parameters
                    $spreadsheet2->getActiveSheet()->getPageSetup()->setFitToPage(True);
                    $spreadsheet2->getActiveSheet()->getPageMargins()->setTop(0);
                    $spreadsheet2->getActiveSheet()->getPageMargins()->setRight(0);
                    $spreadsheet2->getActiveSheet()->getPageMargins()->setLeft(0);
                    $spreadsheet2->getActiveSheet()->getPageMargins()->setBottom(0);

		            //Save the file for the object
                    $writer = new writer\Xlsx($spreadsheet2);
                    $writer->save($NomFichier);


                    //SavePDF for object
                    $writer2 = new \PhpOffice\PhpSpreadsheet\Writer\Pdf\Mpdf($spreadsheet2);
                    $writer2->writeAllSheets();
                    $writer2->save($NomFichierPDF);
                }
            } elseif($selectedType->Type == 'Comparatif') {
                //To do

            } elseif($selectedType->Type == 'Map') {
                //To do
            }
        }

        $count = count($exportSelected);
        return view('export', [
            'userId' => $userId,
            'count' => $count,
            'credit' => $credit,
            'actePDF' => $actePDF,
            'exportSelected' => $exportSelected
        ]);
        //return response()->download(storage_path('Output/RapportPDF_' . $userId . '.pdf'));
    }

    public function exportPDF(Request $request)
    {
        $userId = Auth::user()->id;
        $exportSelected = $request->input('exportSelected');
        $pdfMerger = PDFMerger::init();
        $NomFichierFinalPDF = storage_path('app/Output/RapportPDF_' . $userId . '.pdf');
        foreach ($exportSelected as $selected) {
            $NomFichierPDF = storage_path('app/Output/FicheBrute_' . $userId . '_' . $selected['reporttype'] . '_' . $selected['ObjID'] . '.pdf');
            //Prepare the list for compilation
            $pdfMerger->addPDF($NomFichierPDF, 'all');
        }
        $pdfMerger->merge();
        $pdfMerger->save($NomFichierFinalPDF, 'file');
        return response()->download($NomFichierFinalPDF);
    }

    public function exportExcel(Request $request)
    {
        $userId = Auth::user()->id;
        $exportSelected = $request->input('exportSelected');
        //open the final spreadsheet
        $spreadsheetFinale=new Spreadsheet();
        $NomFichierFinal = storage_path('app/Output/Rapport_' . $userId . '.xlsx');
        foreach ($exportSelected as $selected) {
            $NomFichier = storage_path('app/Output/FicheBrute_' . $userId . '_' . $selected['reporttype'] . '_' . $selected['ObjID'] . '.xlsx');
            //Open the Model
            $reader = new reader\Xlsx();
            $spreadsheet = $reader->load($NomFichier);
            $sheetNames = $spreadsheet->getSheetNames();
            foreach ($sheetNames as $sheet) {
                $OutputSheet = $sheet . '_' . $selected['reporttype'] . '_' . $selected['ObjID'];
                $cloneWorksheet = clone $spreadsheet->getSheetByName($sheet);
                $spreadsheetFinale->addExternalSheet($cloneWorksheet);
                $sheet = $spreadsheetFinale->setActiveSheetIndexByName($sheet);
                $sheet->setTitle($OutputSheet);
            }
        }
        $spreadsheetFinale->setActiveSheetIndexByName('Worksheet');
        $sheetIndex = $spreadsheetFinale->getActiveSheetIndex();
        $spreadsheetFinale->removeSheetByIndex($sheetIndex);
        $writerFinal = new writer\Xlsx($spreadsheetFinale);
        $writerFinal->save($NomFichierFinal);

        return response()->download($NomFichierFinal);
    }

    public function buyWithCredit(Request $request)
    {
        $userId = Auth::user()->id;
        $price = $request->input('price');
        $result = DB::select('CALL Proc_BuyWCredit(?,?)', array($userId, $price))[0];
        return response()->json($result);
    }

    public function exportActePdf(Request $request)
    {
        $file = $request->input('file');
        $FileName = 'Recherche/Files/' . $file;
        $FileGet = Storage::disk('s3')->get($FileName);
        Storage::put('temp/' . $file, $FileGet);
        return response()->download(storage_path('app/temp/'.$file))->deleteFileAfterSend();
    }

    public function AssessRecherchePrice(Request $request)
    {
        $userId = Auth::user()->id;
        $ResearchType = $request->input('ResearchType');
        $price = DB::select('CALL Proc_AssessRecherchePrice(?,?)', array($userId, $ResearchType))[0];
        return response()->json($price);
    }

    public function BuyRecherche(Request $request)
    {
        $userId = Auth::user()->id;
        $ResearchType = $request->input('ResearchType');
        $result = DB::select('CALL Proc_BuyRecherche(?,?)', array($userId, $ResearchType))[0];
        return response()->json($result);
    }

    public function AssessProduitPDF(Request $request)
    {
        $userId = Auth::user()->id;
        $itemID = $request->input('itemID');
        $price = DB::select('CALL Proc_AssessProduitPDF(?,?)', array($userId, $itemID))[0];
        return response()->json($price);
    }

    public function Proc_AssessProduitPDFSelection(Request $request)
    {
        $userId = Auth::user()->id;
        $exportType = $request->input('exportType');
        DB::statement('call Proc_ClearSelection(?)',array($userId));
        foreach ($exportType as $export) {
            DB::statement('call Proc_BuildSelection(?,?)',array($userId, $export['id']));
        }
        $price = DB::select('CALL Proc_AssessProduitPDFSelection(?)', array($userId))[0];
        return response()->json($price);
    }

    public function AssessProduitExcelDirect(Request $request)
    {
        $userId = Auth::user()->id;
        $itemID = $request->input('itemID');
        $price = DB::select('CALL Proc_AssessProduitExcelDirect(?,?)', array($userId, $itemID))[0];
        return response()->json($price);
    }

    public function BuyPDF(Request $request)
    {
        $userId = Auth::user()->id;
        $reportType = $request->input('reportType');
        $result = DB::select('CALL Proc_BuyPDF(?,?)', array($userId, $reportType))[0];
        return response()->json($result);
    }

    public function BuyDonneeUnique(Request $request)
    {
        $userId = Auth::user()->id;
        $ObjID = $request->input('ObjID');
        $ChampNomGen = $request->input('ChampNomGen');
        $result = DB::select('CALL Proc_BuyDonneeUnique(?,?,?)', array($userId, $ChampNomGen, $ObjID))[0];
        return response()->json($result);
    }

    public function SelectExcel(Request $request)
    {
        $userId = Auth::user()->id;
        $id = $request->input('id');
        $reportType = $request->input('reportType');
        DB::select('CALL Proc_SelectExcel(?,?,?)', array($userId, $id, $reportType));
        return response()->json(array('success' => true));
    }

    public function AssessProduitExcelSelection(Request $request)
    {
        $userId = Auth::user()->id;
        $price = DB::select('CALL Proc_AssessProduitExcelSelection(?)', array($userId))[0];
        return response()->json($price);
    }

    public function BuyExcelSelection(Request $request)
    {
        $userId = Auth::user()->id;
        $price = DB::select('CALL Proc_BuyExcelSelection(?)', array($userId))[0];
        return response()->json($price);
    }

    public function newPage()
    {
        $userId = Auth::user()->id;
        $files = DB::select('CALL Proc_RequestPropertyDL(?)', array($userId));
        return view('new',[
            'files' => $files
        ]);
    }

    public function RequestProperty(Request $request)
    {
        $userId = Auth::user()->id;
        $number = $request->input('number');
        $address = $request->input('address');
        $comment = $request->input('comment');
        DB::statement('call Proc_RequestProperty(?,?,?,?)',array($number, $address, $comment, $userId));
        return true;
    }

    public function PropertyDL_Gotit(Request $request)
    {
        $userId = Auth::user()->id;
        $fileName = $request->input('fileName');
        DB::statement('call Proc_PropertyDL_Gotit(?,?)',array($fileName, $userId));
        return true;
    }
}
