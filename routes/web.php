<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' => 'auth'], function () {
    Route::redirect('/home', '/dashboard', 301);
    Route::get('/dashboard', 'HomeController@index')->name('home');
    Route::post('/dashboard/resultCount', 'HomeController@getResultCount')->name('home.resultCount');
    Route::get('/dashboard/displayResult', 'HomeController@displayResult')->name('home.displayResult');
    Route::post('/dashboard/selectResult', 'HomeController@selectResult')->name('home.selectResult');
    Route::post('/dashboard/exportPDF', 'HomeController@exportPDF')->name('home.exportPDF');
    Route::post('/dashboard/exportExcel', 'HomeController@exportExcel')->name('home.exportExcel');
    Route::get('/file/{directory}/{slug}', 'FileController@get')->name('file.get');
    Route::get('/dashboard/getListValues', 'HomeController@getListValues')->name('home.getListValues');
    Route::get('/dashboard/exportPage', 'HomeController@exportPage')->name('home.exportPage');
    Route::post('/dashboard/buyWithCredit', 'HomeController@buyWithCredit')->name('home.buyWithCredit');
    Route::post('/dashboard/exportActePdf', 'HomeController@exportActePdf')->name('home.exportActePdf');
    Route::post('/dashboard/AssessRecherchePrice', 'HomeController@AssessRecherchePrice')->name('home.AssessRecherchePrice');
    Route::post('/dashboard/BuyRecherche', 'HomeController@BuyRecherche')->name('home.BuyRecherche');
    Route::post('/dashboard/AssessProduitPDF', 'HomeController@AssessProduitPDF')->name('home.AssessProduitPDF');
    Route::post('/dashboard/Proc_AssessProduitPDFSelection', 'HomeController@Proc_AssessProduitPDFSelection')->name('home.Proc_AssessProduitPDFSelection');
    Route::post('/dashboard/AssessProduitExcelDirect', 'HomeController@AssessProduitExcelDirect')->name('home.AssessProduitExcelDirect');
    Route::post('/dashboard/BuyPDF', 'HomeController@BuyPDF')->name('home.BuyPDF');
    Route::post('/dashboard/BuyExcelDirect', 'HomeController@BuyExcelDirect')->name('home.BuyExcelDirect');
    Route::post('/dashboard/SelectExcel', 'HomeController@SelectExcel')->name('home.SelectExcel');
    Route::post('/dashboard/BuyExcelSelection', 'HomeController@BuyExcelSelection')->name('home.BuyExcelSelection');
    Route::post('/dashboard/AssessProduitExcelSelection', 'HomeController@AssessProduitExcelSelection')->name('home.AssessProduitExcelSelection');
    Route::post('/dashboard/BuyDonneeUnique', 'HomeController@BuyDonneeUnique')->name('home.BuyDonneeUnique');

    Route::get('/new', 'HomeController@newpage')->name('newpage');
    Route::post('/new/RequestProperty', 'HomeController@RequestProperty')->name('home.RequestProperty');
    Route::post('/new/PropertyDL_Gotit', 'HomeController@PropertyDL_Gotit')->name('home.PropertyDL_Gotit');
});
