@extends('adminlte::page')

@section('css')
<style>
    .dashboard_header {
        display: flex;
        justify-content: space-between;
    }
</style>
@stop

@section('title', 'Export')

@section('content_header')
<div class="dashboard_header">
    <span class="logo-lg"><img src="{{asset('/file/temp/cmsd.png')}}" height="40px"></span>
    <h3 class="credit-box">Your credit:<span class="credit">{{ $credit->CreditNumber }}</span></h3>
</div>
@stop
@section('content')
    <div class="row" data-number="0" data-ObjID="{{ $exportSelected[0]['ObjID'] }}" id="main">
        <div class="col-md-12 text-center">
            <h4><span class="pdf_number">1</span>/{{ $count }}</h4>
        </div>
        <div class="col-md-9">
            <iframe src = "{{asset('/file/Output/FicheBrute_' . $userId . '_' . $exportSelected[0]['reportType'] . '_' . $exportSelected[0]['ObjID'] . '.pdf')}}" class="pdf" style="width: 100%; height: 100vh;"></iframe>
        </div>
        <div class="col-md-2">
            <div class="d-none">
                @foreach ($exportSelected as $key => $item)
                    <input type="checkbox" value="{{ $item['ObjID'] }}" data-reporttype="{{ $item['reportType'] }}" class="pdf_check" checked data-number="{{ $key }}">
                    <input type="checkbox" value="{{ $item['ObjID'] }}" data-reporttype="{{ $item['reportType'] }}" class="excel_check" checked data-number="{{ $key }}">
                @endforeach
            </div>
            <input type="button" value="Sélectionner pour le rapport" class="btn btn-block btn-success pdf_select">
            <input type="button" value="Sélectionné pour l'exportation exel" class="btn btn-block btn-success excel_select">
            <input type="button" value="Obtenir le rapport" class="btn btn-block btn-primary pdf_export">
            <input type="button" value="Exporter les fiches sur excel" class="btn btn-block btn-primary excel_export">
            <input type="button" value="Précédent" class="btn btn-block btn-default prev" @if ($count == 1) disabled @endif>
            <input type="button" value="Suivant" class="btn btn-block btn-default next" @if ($count == 1) disabled @endif>
            <button class="btn btn-block btn-default acte_pdf" {{ $actePDF[$exportSelected[0]['ObjID']]['status'] }}><img src="{{asset('/file/temp/pdf.png')}}" style="height:30px"></button>
        </div>
        <div class="col-md-1">
            <div class="btn btn-default btn-block">
                <span class="pdf_selected">{{ $count }}</span>/{{ $count }}
            </div>
            <div class="btn btn-default btn-block">
                <span class="excel_selected">{{ $count }}</span>/{{ $count }}
            </div>
        </div>
    </div>
@stop
@section('js')
<script>
    <?php
        $js_actes = json_encode($actePDF);
        echo "var actePDF = ". $js_actes . ";\n";
    ?>
    var excel_selected = {{ $count }};
    var pdf_selected = {{ $count }};
    var count = {{ $count }};
    $('.pdf_select').on('click', function(){
        var number = $('#main').data('number');
        if($(this).hasClass('btn-success'))
        {
            $('.pdf_check[data-number="' + number + '"]').prop('checked', false);
            $(this).removeClass('btn-success');
            $(this).addClass('btn-secondary');
            $(this).val('Ajourté à la sélection pour le rapport');
            pdf_selected--;
            $('.pdf_selected').html(pdf_selected);
        } else {
            $('.pdf_check[data-number="' + number + '"]').prop('checked', true);
            $(this).addClass('btn-success');
            $(this).removeClass('btn-secondary');
            $(this).val('Sélectionner pour le rapport');
            pdf_selected++;
            $('.pdf_selected').html(pdf_selected);
        }
    });
    $('.excel_select').on('click', function(){
        var number = $('#main').data('number'),
            excel_select = $(this)
            selected = $('.excel_check[data-number="' + number + '"]');
        $.ajax({
            type: 'POST',
            url: "{{ route('home.SelectExcel') }}",
            data: {
                id: selected.val(),
                reportType: selected.data('reporttype'),
                _token: "{{ csrf_token() }}",
            },
            dataType: 'json'
        })
        .done(function (data) {
            if(data.success)
            {
                if(excel_select.hasClass('btn-success'))
                {
                    $('.excel_check[data-number="' + number + '"]').prop('checked', false);
                    excel_select.removeClass('btn-success');
                    excel_select.addClass('btn-secondary');
                    excel_select.val("Ajouter à la sélection pour l'exportation excel");
                    excel_selected--;
                    $('.excel_selected').html(excel_selected);
                } else {
                    $('.excel_check[data-number="' + number + '"]').prop('checked', true);
                    excel_select.addClass('btn-success');
                    excel_select.removeClass('btn-secondary');
                    excel_select.val("Sélectionné pour l'exportation exel");
                    excel_selected++;
                    $('.excel_selected').html(excel_selected);
                }
            }
        });
    });
    $('.prev').on('click', function(){
        var number = $('#main').data('number');
        var selected = $('.pdf_check[data-number="' + number + '"]');
        number--;
        if(number < 0)
        {
            alert('This is the first one!!');
        } else {
            console.log(number);
            $('.pdf_number').html(number + 1);
            var pdf = $('.pdf_check[data-number="' + number + '"]');
            var excel = $('.excel_check[data-number="' + number + '"]');
            var ObjID = pdf.val();
            var reporttype = pdf.data('reporttype');
            $('#main').data('number', number);
            $('#main').data('objid', ObjID);
            var pdf_link = "{{asset('/file/Output/')}}" + "/FicheBrute_" + "{{ $userId }}" + "_" + reporttype + "_" + ObjID + '.pdf';
            $('.pdf').attr('src', pdf_link);
            $('.acte_pdf').prop('disabled', actePDF[ObjID]['status']);
            if(pdf.is(':checked'))
            {
                $('.pdf_select').addClass('btn-success');
                $('.pdf_select').removeClass('btn-secondary');
                $('.pdf_select').val('Sélectionner pour le rapport');
            } else {
                $('.pdf_select').removeClass('btn-success');
                $('.pdf_select').addClass('btn-secondary');
                $('.pdf_select').val('Ajourté à la sélection pour le rapport');
            }
            if(excel.is(':checked'))
            {
                $('.excel_select').addClass('btn-success');
                $('.excel_select').removeClass('btn-secondary');
                $('.excel_select').val("Sélectionné pour l'exportation exel");
            } else {
                $('.excel_select').removeClass('btn-success');
                $('.excel_select').addClass('btn-secondary');
                $('.excel_select').val("Ajouter à la sélection pour l'exportation excel");
            }
        }
    });
    $('.next').on('click', function(){
        var number = $('#main').data('number');
        var selected = $('.pdf_check[data-number="' + number + '"]');
        number++;
        if(number == count)
        {
            alert('This is the last one!!');
        } else {
            console.log(number);
            $('.pdf_number').html(number + 1);
            var pdf = $('.pdf_check[data-number="' + number + '"]');
            var excel = $('.excel_check[data-number="' + number + '"]');
            var ObjID = pdf.val();
            var reporttype = pdf.data('reporttype');
            $('#main').data('number', number);
            $('#main').data('objid', ObjID);
            var pdf_link = "{{asset('/file/Output/')}}" + "/FicheBrute_" + "{{ $userId }}" + "_" + reporttype + "_" + ObjID + '.pdf';
            $('.pdf').attr('src', pdf_link);
            $('.acte_pdf').prop('disabled', actePDF[ObjID]['status']);
            if(pdf.is(':checked'))
            {
                $('.pdf_select').addClass('btn-success');
                $('.pdf_select').removeClass('btn-secondary');
                $('.pdf_select').val('Sélectionner pour le rapport');
            } else {
                $('.pdf_select').removeClass('btn-success');
                $('.pdf_select').addClass('btn-secondary');
                $('.pdf_select').val('Ajourté à la sélection pour le rapport');
            }
            if(excel.is(':checked'))
            {
                $('.excel_select').addClass('btn-success');
                $('.excel_select').removeClass('btn-secondary');
                $('.excel_select').val("Sélectionné pour l'exportation exel");
            } else {
                $('.excel_select').removeClass('btn-success');
                $('.excel_select').addClass('btn-secondary');
                $('.excel_select').val("Ajouter à la sélection pour l'exportation excel");
            }
        }
    });
    $('.pdf_export').on('click', function(){
        var exportSelected = [];
        $('.pdf_check').each(function(){
            if(this.checked)
            {
                var type = {
                    'ObjID':  $(this).val(),
                    'reporttype': $(this).data('reporttype'),
                }
                exportSelected.push(type);
            }
        });
        var data = {
            _token: "{{ csrf_token() }}",
            exportSelected: exportSelected,
        };
        fetch("{{ route('home.exportPDF') }}", {
            body: JSON.stringify(data),
            method: 'POST',
            headers: {
                'Content-Type': 'application/json; charset=utf-8'
            },
        })
        .then(response => response.blob())
        .then(response => {
            const blob = new Blob([response], {type: 'application/pdf'});
            const downloadUrl = URL.createObjectURL(blob);
            const a = document.createElement("a");
            a.href = downloadUrl;
            a.download = "Result.pdf";
            document.body.appendChild(a);
            a.click();
        });
    });
    $('.excel_export').on('click', function(){
        var excel_btn = $(this),
            button_title = excel_btn.val();
        var exportSelected = [];
        $('.excel_check').each(function(){
            if(this.checked)
            {
                var type = {
                    'ObjID':  $(this).val(),
                    'reporttype': $(this).data('reporttype'),
                }
                exportSelected.push(type);
            }
        });
        var temp_reporttypes = exportSelected.map(value => value.reporttype);
        var reporttypes = [];
        $.each(temp_reporttypes, function(i, el){
            if($.inArray(el, reporttypes) === -1) reporttypes.push(el);
        });
        if(button_title == 'Exporter les fiches sur excel')
        {
            $.ajax({
                type: 'POST',
                url: "{{ route('home.AssessProduitExcelSelection') }}",
                data: {
                    _token: "{{ csrf_token() }}",
                },
                dataType: 'json'
            })
            .done(function(data) {
                excel_btn.val("Procéder pour " + data.Price + " credits");
            });
        } else {
            $.ajax({
                type: 'POST',
                url: "{{ route('home.BuyExcelSelection') }}",
                data: {
                    _token: "{{ csrf_token() }}",
                },
                dataType: 'json'
            })
            .done(function(data) {
                if(data.Response == 0)
                {
                    alert(data.ResponseTest);
                    return;
                } else {
                    $('.credit').html(data.Solde);
                    var data = {
                        _token: "{{ csrf_token() }}",
                        exportSelected: exportSelected,
                    };
                    fetch("{{ route('home.exportExcel') }}", {
                        body: JSON.stringify(data),
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8'
                        },
                    })
                    .then(response => response.blob())
                    .then(response => {
                        excel_btn.val("Exporter les fiches sur excel");
                        const blob = new Blob([response], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
                        const downloadUrl = URL.createObjectURL(blob);
                        const a = document.createElement("a");
                        a.href = downloadUrl;
                        a.download = "Result.xlsx";
                        document.body.appendChild(a);
                        a.click();
                    });
                }
            });
        }
    });

    $('.acte_pdf').on('click', function(){
        var ObjID = $('#main').data('objid');
        var data = {
            _token: "{{ csrf_token() }}",
            file: actePDF[ObjID]['file'],
        };
        fetch("{{ route('home.exportActePdf') }}", {
            body: JSON.stringify(data),
            method: 'POST',
            headers: {
                'Content-Type': 'application/json; charset=utf-8'
            },
        })
        .then(response => response.blob())
        .then(response => {
            const blob = new Blob([response], {type: 'application/pdf'});
            const downloadUrl = URL.createObjectURL(blob);
            const a = document.createElement("a");
            a.href = downloadUrl;
            a.download = "Acte.pdf";
            document.body.appendChild(a);
            a.click();
        });
    });
</script>
@stop
