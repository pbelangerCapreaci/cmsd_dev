<table class="table table-head-fixed text-nowrap">
    <thead>
        <tr>
            <th><input type="checkbox" id="select_all"/></th>
            @foreach ($resultFields as $item)
                <th @if ($item->ChampExplication) title="{{ $item->ChampExplication }}" @endif>{{ $item->ChampNom }}</th>
            @endforeach
        </tr>
    </thead>
    <tbody>
        @foreach ($results as $item)
            <tr>
                <td>
                    <input type="checkbox" class="result_check" name="{{ $item->ObjID }}" />
                </td>
                @foreach ($resultFields as $field)
                    <td>
                        @if ($field->Pricing > 0)
                            @if ($field->Type == "PDF")
                                <span class="unique_buy" id="{{ $item->ObjID }}" name="{{ $field->ChampNomGen }}" style="font-weight:bold;color:blue">Téléchargez pour {{ $field->Pricing }} credits</span>
                            @else
                                <span class="unique_buy" id="{{ $item->ObjID }}" name="{{ $field->ChampNomGen }}" style="font-weight:bold;color:blue">Accéder à l’information, {{ $field->Pricing }} crédit</span>
                            @endif
                        @endif
                        @if ($field->Type == "IMG")
                            <img src="{{asset('/file/temp/' . $item->{$field->Champ})}}" alt="" width="100px" @if($field->Pricing > 0) class="d-none" @endif>
                        @elseif ($field->Type == "PDF")
                            <a download="{{ $item->{$field->Champ} }}" href="{{asset('/file/temp/' . $item->{$field->Champ})}}" @if($field->Pricing > 0) class="d-none" @endif><img src="{{asset('/file/temp/pdf.png')}}" alt="" width="20px"></a>
                        @else
                            <span @if($field->Pricing > 0) class="d-none" @endif>{{ $item->{$field->Champ} }}</span>
                        @endif
                    </td>
                @endforeach
            </tr>
        @endforeach
    </tbody>
</table>
