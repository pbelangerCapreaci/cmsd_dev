@extends('adminlte::page')

@section('css')
<style>
    .card {
        background-color: #E2E1E2;
    }
    .dashboard_header {
        display: flex;
        justify-content: space-between;
    }
    .navbar-primary {
        background-color: #212736;
    }
</style>
@stop

@section('title', 'CMsd')

@section('content_header')
    <div class="dashboard_header">
        <span class="logo-lg">Ouverture de dossiers</span>
    </div>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header with-border">
                    <input type="button" value="Reset" class="reset_btn">
                </div>
                <div class="card-body">
                    <h3>Information de recherche</h3>
                    <div class="row col-md-12">
                        <div class="form-group col-md-3">
                            <label class="research-label">
                                Numero de lot
                            </label>
                            <input type="text" class="form-control filter_input number">
                        </div>
                        <div class="form-group col-md-3">
                            <label class="research-label">
                                Adresse
                            </label>
                            <input type="text" class="form-control filter_input address">
                        </div>
                        <div class="form-group col-md-6">
                            <label class="research-label">
                                Commentaire
                            </label>
                            <textarea class="form-control filter_input" cols="30" rows="5"></textarea>
                        </div>
                        <input type="button" value="Add New" class="add_new float-right comment">
                    </div>
                    <h3>Dossiers ouverts</h3>
                    <table class="table table-head-fixed text-nowrap">
                        <thead>
                            <tr>
                                <th>Numeros de lot</th>
                                <th>Adresse</th>
                                <th>PDF</th>
                                <th>Excel</th>
                                <th>Date de depot</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($files as $file)
                                <tr>
                                    <td>
                                        <span>{{ $file->LotNumber }}</span>
                                    </td>
                                    <td>
                                        <span>{{ $file->Adresse }}</span>
                                    </td>
                                    <td>
                                        @if ($file->PDFFileName != null)
                                        <a href="{{asset('/file/temp/' . $file->PDFFileName)}}" class="file_download" data-filename="{{ $file->PDFFileName }}"><img src="{{asset('/file/temp/pdf.png')}}" alt="" width="20px"></a>
                                        @else
                                            <img src="{{asset('/file/temp/gray-pdf.png')}}" alt="" width="20px">
                                        @endif
                                    </td>
                                    <td>
                                        @if ($file->ExcelFileName != null)
                                        <a href="{{asset('/file/temp/' . $file->ExcelFileName)}}" class="file_download" data-filename="{{ $file->ExcelFileName }}"><img src="{{asset('/file/temp/excel.png')}}" alt="" width="20px"></a>
                                        @else
                                            <img src="{{asset('/file/temp/gray-excel.png')}}" alt="" width="20px">
                                        @endif
                                    </td>
                                    <td>
                                        <span>{{ $file->DepotDate }}</span>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
<script>
    $('.reset_btn').on('click', function(){
        $('.filter_input').val('');
        $('.filter_input').change();
    });
    $('.add_new').on('click', function(){
        var number = $('.number').val(),
            address = $('.address').val(),
            comment = $('.comment').val();
        $.ajax({
            type: 'POST',
            url: "{{ route('home.RequestProperty') }}",
            data: {
                number: number,
                address: address,
                comment: comment,
                _token: "{{ csrf_token() }}",
            },
            dataType: 'json'
        })
        .done(function (data) {
            var tr_html = '<tr><td><span>' + number + '</span></td><td><span>' + address + '</span></td><td><img src="' + "{{asset('file/temp/gray-pdf.png')}}" + '"alt="" width="20px"></td><td><img src="'+ "{{asset('/file/temp/gray-excel.png')}}" + '"alt="" width="20px"></td><td><span>A venir</span></td></tr>';
            $('.table tbody tr').last().after(tr_html);
            console.log('added');
        });
    });
    $('.file_download').on('click', function(e){
        e.preventDefault();
        var link = $(this).attr('href'),
            fileName = $(this).data('filename');
        $.ajax({
            type: 'POST',
            url: "{{ route('home.PropertyDL_Gotit') }}",
            data: {
                fileName: fileName,
                _token: "{{ csrf_token() }}",
            },
            dataType: 'json'
        })
        .done(function (data) {
            console.log('downloaded');
            window.location.href = link;
        });
    });
</script>
@stop
