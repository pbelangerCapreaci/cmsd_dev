@extends('adminlte::page')

@section('css')
<style>
    .card {
        background-color: #E2E1E2;
    }
    .dashboard_header {
        display: flex;
        justify-content: space-between;
    }
    .navbar-primary {
        background-color: #212736;
    }
    .tooltip {
        text-align: center;
        background: #333;
        background: rgba(#333, 0.9);
        color: #fff;
        bottom: 100%;
        padding: 5px 10px;
        display: block;
        position: absolute;
        width: 120px;
        font-size: 13px;
        left: -35px;
        margin-bottom: 15px;
        opacity: 0;
        visibility: hidden;
        transform: translateY(10px);
        transition: all .25s ease-out;
        box-shadow: 2px 2px 6px rgba(0, 0, 0, 0.28);
        z-index: 60;
    }
    .tooltip:before {
        bottom: -20px;
        content: " ";
        display: block;
        height: 20px;
        left: 0;
        position: absolute;
        width: 100%;
    }
    .tooltip:after {
        border-left: solid transparent 10px;
        border-right: solid transparent 10px;
        border-top: solid rgba(51,51,51,0.9) 10px;
        bottom: -10px;
        content: " ";
        height: 0;
        left: 50%;
        margin-left: -13px;
        position:absolute;
        width: 0;
    }
    .research-label:hover .tooltip {
        opacity:1;
        visibility:visible;
        transform:translateY(0px);
    }
</style>
@stop

@section('title', 'CMsd')

@section('content_header')
    <div class="dashboard_header">
        <span class="logo-lg"><img src="research.png" height="40px"></span>
        <h3 class="credit-box">Your credit:<span class="credit">{{ $credit->CreditNumber }}</span></h3>
    </div>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header with-border">
                    <input type="button" value="Reset" class="reset_btn">
                </div>
                <div class="card-body">
                    @foreach ($layoutByCategory as $layout)
                        <h3>{{ $layout[0]->CategorieNom }}</h3>
                        <div class="row col-md-12">
                            @foreach ($layout as $item)
                                @switch($item->Type)
                                    @case('List')
                                        <div class="form-group col-md-{{ $item->Taille }}">
                                            <label class="research-label">
                                                @if ($item->Explication)
                                                    <div class="tooltip">{{ $item->Explication }}</div>
                                                @endif
                                                {{ $item->ChampNom }}
                                            </label>
                                            <select name="{{ $item->ChampNomGen }}" class="form-control filter_input filter_list" >
                                                <option value=""></option>
                                                @foreach ($item->list as $list_item)
                                                    <option value="{{ $list_item->Valeur }}">{{ $list_item->Valeur }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        @break
                                    @case('Value')
                                        <div class="form-group col-md-{{ $item->Taille }}">
                                            <label class="research-label">
                                                @if ($item->Explication)
                                                    <div class="tooltip">{{ $item->Explication }}</div>
                                                @endif
                                                {{ $item->ChampNom }}
                                            </label>
                                            <input type="text" class="form-control filter_input" name="{{ $item->ChampNomGen }}">
                                        </div>
                                        @break
                                    @case('IntervalleDate')
                                        <div class="form-group col-md-{{ $item->Taille }}">
                                            <label class="research-label">
                                                @if ($item->Explication)
                                                    <div class="tooltip">{{ $item->Explication }}</div>
                                                @endif
                                                {{ $item->ChampNom }}
                                            </label>
                                            <div class="d-flex">
                                                <input type="text" class="form-control IntervalleDate filter_input min_value" name="{{ $item->ChampNomGen }}" autocomplete="off">
                                                et
                                                <input type="text" class="form-control IntervalleDate filter_input max_value" name="{{ $item->ChampNomGen }}" autocomplete="off"  >
                                            </div>
                                        </div>
                                        @break
                                    @case('Intervalle')
                                        <div class="form-group col-md-{{ $item->Taille }}">
                                            <label class="research-label">
                                                @if ($item->Explication)
                                                    <div class="tooltip">{{ $item->Explication }}</div>
                                                @endif
                                                {{ $item->ChampNom }}
                                            </label>
                                            <div class="d-flex">
                                                <input type="text" class="form-control filter_input min_value" name="{{ $item->ChampNomGen }}" style="width: 49%">
                                                et
                                                <input type="text" class="form-control filter_input max_value" name="{{ $item->ChampNomGen }}" style="width: 49%">
                                            </div>
                                        </div>
                                        @break
                                @endswitch
                            @endforeach
                        </div>
                    @endforeach
                </div>
                <div class="card-footer">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label style="width:100%">Type de recherche désiré</label>
                                    @foreach ($researchType as $item)
                                        <input type="button" class="col-md-3 btn btn-info result_btn" value="{{ $item->RechercheNom }}" name="{{ $item->RechercheNumber }}" price="{{ $item->Prix }}" title="{{ $item->RechercheExplication }}" >
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label class="col-md-12">Nombre de resultats selon vos</label>
                                    <input type="text" id="total_count" disabled>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="col-md-12">Nombre de resultats qui seront affiches</label>
                                    <input type="text" id="result_count" value="{{ $limit->LimiteResultat }}" disabled>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card result-card">
                <div class="card-header with-border">
                    <h3 class="card-title">Result</h3>
                </div>
                <div class="card-body table-responsive p-0 result" style="height: 500px">
                </div>
                <div class="overlay dark">
                    <span class="no_data">No Data Yet</span>
                    <i class="fas fa-2x fa-sync-alt fa-spin d-none"></i>
                </div>
                <div class="card-footer">
                    <div class="result-model"></div>
                    <input type="button" value="Exporter" class="col-md-3 btn btn-info export_btn" disabled>
                    <input type="button" value="Exporter Excel" class="col-md-3 btn btn-info excel_export" disabled>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
<script>
    var selectedCount = 0;
    <?php
        $research_buttons = json_encode($researchType);
        echo "var researchButtons = ". $research_buttons . ";\n";
    ?>
    $(document).ready(function() {
        $('.IntervalleDate').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "yy-mm-dd"
        });
        var myDate = new Date();
        var oldDate = new Date('2018/01/01');
        var max = myDate.getFullYear() + '-' + ('0'+ (myDate.getMonth()+1)).slice(-2) + '-' + ('0'+ myDate.getDate()).slice(-2);
        var min = (oldDate.getFullYear()) + '-' + ('0'+ (oldDate.getMonth()+1)).slice(-2) + '-' + ('0'+ oldDate.getDate()).slice(-2);
        $(".IntervalleDate.min_value").val(min);
        $(".IntervalleDate.max_value").val(max);
    });
    $('.reset_btn').on('click', function(){
        $('.filter_input').val('');
        var myDate = new Date();
        var oldDate = new Date('2018/01/01');
        var max = myDate.getFullYear() + '-' + ('0'+ (myDate.getMonth()+1)).slice(-2) + '-' + ('0'+ myDate.getDate()).slice(-2);
        var min = (oldDate.getFullYear()) + '-' + ('0'+ (oldDate.getMonth()+1)).slice(-2) + '-' + ('0'+ oldDate.getDate()).slice(-2);
        //$('#total_count').val('');
        $('.filter_input').change();
    });
    $('.filter_input').on('change', function(e){
        var filterData = {};
        filterData['ChampNomGen'] = $(this).attr('name');
        if($(this).hasClass('min_value')){
            filterData['min'] = $(this).val();
            filterData['max'] = $(this).parent().parent().find('.max_value').val();
        } else if($(this).hasClass('max_value')) {
            filterData['max'] = $(this).val();
            filterData['min'] = $(this).parent().parent().find('.min_value').val();
        } else {
            filterData['min'] = $(this).val();
            filterData['max'] = null;
        }
        var data = {
            filterData: filterData,
            _token: "{{ csrf_token() }}",
        };
        $.ajax({
            type: 'POST',
            url: "{{ route('home.resultCount') }}",
            data: data,
            dataType: 'json'
        })
        .done(function (data) {
            $('#total_count').val(data);
            $('.result_btn').each(function(){
                var resultBtn = $(this),
                    title = resultBtn.val(),
                    researchType = resultBtn.attr('name');
                if(title.indexOf("credit") !== -1) {
                    var buttonTitle = researchButtons[researchType]['RechercheNom'];
                    resultBtn.val(buttonTitle);
                }
            });
        });
    });
    $('.result_btn').on('click', function(e){
        var resultBtn = $(this),
            title = resultBtn.val(),
            researchType = resultBtn.attr('name');
        if(title.indexOf("credit") !== -1)
        {
            $.ajax({
                type: 'POST',
                url: "{{ route('home.BuyRecherche') }}",
                data: {
                    ResearchType: researchType,
                    _token: "{{ csrf_token() }}",
                },
                dataType: 'json'
            })
            .done(function (data) {
                var buttonTitle = researchButtons[researchType]['RechercheNom'];
                resultBtn.val(buttonTitle);
                if(data.Response == "0")
                {
                    alert(data.ResponseTest);
                    return;
                } else {
                    $('.credit').html(data.Solde);
                    $('.result-card').find('.overlay').show();
                    $('.no_data').hide();
                    $('.fa-spin').removeClass('d-none');
                    $.ajax({
                        type: 'GET',
                        url: "{{ route('home.displayResult') }}",
                        data: {
                            ResearchType: researchType,
                            _token: "{{ csrf_token() }}",
                        },
                        dataType: 'json'
                    })
                    .done(function (data) {
                        $('.result-card').find('.overlay').hide();
                        $('.result').html(data.resultHtml);
                        $('.result-model').html(data.modelHtml);
                        $('.export_btn').attr('disabled', false);
                        $('.result_check').on('change', resultCheck);
                        $('#select_all').click(function(event) {
                            var that = this;
                            $('.result_check').each(function() {
                                if(this.checked != that.checked)
                                {
                                    $(this).click();
                                }
                            });
                        });
                        $('.unique_buy').on('click', function(){
                            var that = $(this),
                                ObjID = that.attr('id'),
                                ChampNomGen = that.attr('name');
                            $.ajax({
                                type: 'POST',
                                url: "{{ route('home.BuyDonneeUnique') }}",
                                data: {
                                    ObjID: ObjID,
                                    ChampNomGen: ChampNomGen,
                                    _token: "{{ csrf_token() }}",
                                },
                                dataType: 'json'
                            })
                            .done(function (data) {
                                if(data.Response == "0")
                                {
                                    alert(data.ResponseTest);
                                    return;
                                } else {
                                    that.next().removeClass('d-none');
                                    that.hide();
                                }
                            });
                        });
                    });
                }
            });
        } else {
            $.ajax({
                type: 'POST',
                url: "{{ route('home.AssessRecherchePrice') }}",
                data: {
                    ResearchType: researchType,
                    _token: "{{ csrf_token() }}",
                },
                dataType: 'json'
            })
            .done(function (data) {
                var buttonTitle = "Procéder pour " + data.Price + " credits";
                resultBtn.val(buttonTitle);
            });
        }
    });
    resultCheck = function(e) {
        var check = $(e.target);
        $.ajax({
            type: 'POST',
            url: "{{ route('home.selectResult') }}",
            data: {
                id: check.attr('name'),
                _token: "{{ csrf_token() }}",
            },
            dataType: 'json'
        })
        .done(function (data) {
            $('.export_btn').val('Exporter');
            if(data.success)
            {
                if(check.is(":checked"))
                {
                    selectedCount++;
                } else {
                    selectedCount--;
                }
                $('.model_body tr').each(function() {
                    var modelCheck = $(this).find('.model_check'),
                        modelNumber = $(this).find('.model_number'),
                        modelPrice = $(this).find('.model_price'),
                        modelExcelPrice = $(this).find('.model_excel_price'),
                        itemPrice = modelCheck.attr('price'),
                        itemType = modelCheck.attr('name'),
                        itemID = modelCheck.attr('id'),
                        price, number;

                    $.ajax({
                        type: 'POST',
                        url: "{{ route('home.AssessProduitPDF') }}",
                        data: {
                            itemID: itemID,
                            _token: "{{ csrf_token() }}",
                        },
                        dataType: 'json'
                    })
                    .done(function (data) {
                        modelPrice.html(data.Price);
                    });

                        /* $.ajax({
                            type: 'POST',
                            url: "{{ route('home.AssessProduitExcelDirect') }}",
                            data: {
                                itemID: itemID,
                                _token: "{{ csrf_token() }}",
                            },
                            dataType: 'json'
                        })
                        .done(function (data) {
                            modelExcelPrice.html(data.Price);
                        }); */

                    if(itemType == 'Individuel') {
                        number = selectedCount;
                    } else if(itemType == 'Comparatif') {

                        if(selectedCount)
                        {
                            number = 1;
                        } else {
                            number = 0;
                        }
                    }
                    modelNumber.html(number);

                });
            }
        });
    }
    /* $('.export_btn').on('click', function(e){
        var exportType = [], total_price = 0;
        $('.model_check').each(function(){
            if($(this).is(":checked"))
            {
                var type = {
                    'id':  $(this).attr('id'),
                    'Type': $(this).attr('name'),
                }
                total_price += parseInt($(this).parent().parent().find('.model_price').html(), 10);
                exportType.push(type);
            }
        });
        var resultValidate = true;
        $('.result_check').each(function(){
            if($(this).is(":checked"))
            {
                resultValidate = false;
            }
        });
        total_price = 0;
        if(!exportType.length)
        {
            alert('You need to select at least one export type!!!');
        } else if(resultValidate) {
            alert('You need to select at least one result');
        } else {
            $.ajax({
                type: 'POST',
                url: "{{ route('home.buyWithCredit') }}",
                data: {
                    price: total_price,
                    _token: "{{ csrf_token() }}",
                },
                dataType: 'json'
            })
            .done(function (data) {
                console.log(data);
                if(data.Response == 0)
                {
                    alert(data.ResponseTest);
                    return;
                } else {
                    var link = "{{ route('home.exportPage') }}/?exportType=" + encodeURIComponent(JSON.stringify(exportType));
                    var redirectWindow = window.open(link, '_blank');
                    redirectWindow.location;
                }
            });
        }
    }); */
    $('.export_btn').on('click', function(e){
        var exportType = [],
            total_price = 0,
            export_btn = $(this),
            title = export_btn.val(),
            resultValidate = true;

        $('.model_check').each(function(){
            if($(this).is(":checked"))
            {
                var type = {
                    'id':  $(this).attr('id'),
                    'Type': $(this).attr('name'),
                }
                exportType.push(type);
            }
        });

        $('.result_check').each(function(){
            if($(this).is(":checked"))
            {
                resultValidate = false;
            }
        });
        if(!exportType.length)
        {
            alert('You need to select at least one export type!!!');
        } else if(resultValidate) {
            alert('You need to select at least one result');
        } else {
            if(title == "Exporter"){
                $.ajax({
                    type: 'POST',
                    url: "{{ route('home.Proc_AssessProduitPDFSelection') }}",
                    data: {
                        exportType: exportType,
                        _token: "{{ csrf_token() }}",
                    },
                    dataType: 'json'
                })
                .done(function(data){
                    title = "Procéder pour " + data.Price + " credits";
                    export_btn.val(title);
                });
            } else {
                var total_price = parseInt(export_btn.val().split(' ')[2], 10);
                var current_price = parseInt($('.credit').html, 10);
                if(total_price > current_price)
                {
                    alert("You need to charge more credit!");
                } else {
                    var model_checks = $('.model_check:checked');
                    (function loop(i) {
                        var model_check = $(model_checks[i]),
                            reportType = model_check.attr('id');
                        if(i < model_checks.length) {
                            if (i == model_checks.length - 1)
                            {
                                $.ajax({
                                    type: 'POST',
                                    url: "{{ route('home.BuyPDF') }}",
                                    data: {
                                        reportType: reportType,
                                        _token: "{{ csrf_token() }}",
                                    },
                                    dataType: 'json'
                                })
                                .done(function(data) {
                                    if(data.Response == 0)
                                    {
                                        alert(data.ResponseTest);
                                        return;
                                    } else {
                                        export_btn.val('Exporter');
                                        $('.credit').html(data.Solde);
                                        var link = "{{ route('home.exportPage') }}/?exportType=" + encodeURIComponent(JSON.stringify(exportType));
                                        var redirectWindow = window.open(link, '_blank');
                                        redirectWindow.location;
                                    }
                                });
                            } else {
                                $.ajax({
                                    type: 'POST',
                                    url: "{{ route('home.BuyPDF') }}",
                                    data: {
                                        reportType: reportType,
                                        _token: "{{ csrf_token() }}",
                                    },
                                    dataType: 'json'
                                })
                                .done(function(data) {
                                    if(data.Response == 0)
                                    {
                                        alert(data.ResponseTest);
                                        return;
                                    } else {
                                        loop(i + 1);
                                    }
                                });
                            }
                        }
                    })(0);
                }

            }
        }
    });

    var lastFocusValue = '';
    var focusState = 0;

    var changeWithRepeats = function (newestValue) {
    // Your change action here
    };
    $('.filter_list').click (function () {
        if (focusState == 1) { focusState = 2; return; }
        else if (focusState == 2) $(this).blur();
    }).focus(function (e) {
        var that = $(this);
        focusState = 1;
        lastFocusValue = $(this).val();
        $.ajax({
            type: 'GET',
            url: "{{ route('home.getListValues') }}",
            data: {
                listId: $(this).attr('name'),
                _token: "{{ csrf_token() }}",
            },
            dataType: 'json'
        })
        .done(function (data) {
            var list_html = '<option value=""></option>';
            data.forEach(element => {
                if(element.Valeur == lastFocusValue)
                {
                    list_html += '<option value="' + element.Valeur + '" selected>' + element.Valeur + "</option>";
                } else {
                    list_html += '<option value="' + element.Valeur + '">' + element.Valeur + "</option>";
                }

            });
            that.html(list_html);
        });
    }).blur(function () {
        focusState = 0;
        if ($(this).val() == lastFocusValue) {
        // Same value kept in dropdown
        changeWithRepeats($(this).val());
        }
    });
</script>
@stop
