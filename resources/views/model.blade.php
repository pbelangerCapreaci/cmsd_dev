<table class="table table-head-fixed text-nowrap">
    <thead>
        <tr>
            <th>Liste des rapports</th>
            <th>Sélection</th>
            <th>Nombre</th>
            <th>Prix total</th>
            <th>Excel Prix total</th>
        </tr>
    </thead>
    <tbody class="model_body">
        @foreach ($reportTypes as $item)
            <tr>
                <td>{{ $item->RapportNom }}</td>
                <td>
                    <input type="checkbox" class="model_check" id="{{ $item->RapportNumber }}" name="{{ $item->Type }}" price="{{ $item->Prix1 }}" @if($item == $reportTypes[0]) checked @endif/>
                </td>
                <td class="model_number"></td>
                <td class="model_price"></td>
                <td class="model_excel_price"></td>
            </tr>
        @endforeach
    </tbody>
</table>
